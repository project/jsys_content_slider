(function ($) {
    Drupal.behaviors.jsyscontentslideradmin = {
        attach: function (context, settings) {

            let selector_summary_field = '.jsys_summary';
            let selector_summary_enabled_field = 'input' + selector_summary_field + '_enabled';
            let selector_dynamic_field = '.dynamic-field';

            function do_nothing() {}

            function toggle_summary() {
                if ($(selector_summary_enabled_field).is(":checked")) {
                    $(selector_dynamic_field + selector_summary_field).show();
                } else {
                    $(selector_dynamic_field + selector_summary_field).hide();
                }
            }

            function prep_summary_fields() {
                toggle_summary();
                $(selector_summary_enabled_field).click(function () {
                    toggle_summary();
                });
            }

            function toggle_dynamic_field(field_type, selected_field) {
                $(selector_dynamic_field + '.' + field_type).hide();
                $(selector_dynamic_field + '.' + field_type + '.' + selected_field).show();
            }

            function prep_dynamic_select_fields(input_selector, dynamic_field_type, callback = do_nothing) {
                toggle_dynamic_field(dynamic_field_type, $(input_selector).val());
                callback();
                $(input_selector).on('change', function () {
                    toggle_dynamic_field(dynamic_field_type, $(input_selector).val());
                    callback();
                });
            }

            function toggle_delay_field() {
                let delay_field = $('.delay_field');
                if($('select.jsys_effect_type').val() === 'random'){
                    delay_field.hide();
                } else {
                    delay_field.show();
                }
            }

            function init() {
                prep_dynamic_select_fields('select.jsys_content_type', 'content-type');
                prep_dynamic_select_fields('select.jsys_effect_type', 'effect-type', toggle_delay_field);
                prep_summary_fields();
            }
            init();
        }
    };
})(jQuery);