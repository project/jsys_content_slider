(function ($) {
    Drupal.behaviors.jsyscontentslider = {
        attach: function (context, settings) {

            let event_prefix = 'init_jsys_';
            $(document).on(event_prefix + 'gallery', init_gallery);
            $(document).on(event_prefix + 'random', init_random);
            $(document).on(event_prefix + 'animation', init_animation);
            $(document).on(event_prefix + 'slider', init_manual);

            let slider_id = 'slider_id';
            let transition_delay = 'transition_delay';
            let effect_type = 'effect_type';
            let effect_random = ['moveUp', 'moveDown', 'scaleUp'];
            let class_animate_prefix = 'animate__';
            let class_animate_animated = class_animate_prefix + 'animated ';
            let selector_slider_item = '.jsys-slider-item';
            let selector_slider = '.jsys-slider-container';
            let selector_slider_content = ' .jsys-slider-content';
            let event_animation_end = 'animationend';


            // ############################### UTILS ################################## //
            function get_index(index, max) {
                return index + 1 === max ? 0 : index + 1;
            }

            // ############################### ANIMATE.CSS EFFECTS ################################## //
            const animate = (element, animation, hide = false, callback) =>
                new Promise((resolve, reject) => {
                    element.addClass(class_animate_animated + ' ' + animation);
                    element.on(event_animation_end, function animate_end_handler() {
                        if (hide) {
                            element.removeClass(class_animate_animated);
                        }
                        if (callback) {
                            callback();
                        }
                        element.removeClass(animation);
                        element.unbind(event_animation_end);
                        resolve('Animation ended');
                    });
                });

            function animate_start(elements, index, animation_in, animation_out, timeout) {
                let ele = $(elements[index]);
                animate(ele, animation_in).then((message) => {
                    setTimeout(function () {
                        animate(ele, animation_out, true, function () {
                            animate_start(elements, get_index(index, elements.length), animation_in, animation_out, timeout);
                        });
                    }, timeout);
                });
            }

            function init_animation(event, slider_content, slider_setting) {
                let effects = slider_setting['effect_type_value'].split('_');
                let animation_in = class_animate_prefix + effects[0];
                let animation_out = effects[1] ? class_animate_prefix + effects[1] : '';
                animate_start($(slider_content).find(selector_slider_item), 0, animation_in, animation_out, slider_setting[transition_delay]);
            }

            // ############################### RANDOM EFFECTS ################################## //
            function animate_random_end(ele, animation_in) {
                ele.fadeOut().removeClass(animation_in);
            }

            function set_transform_values(parent_container, image_container, animation) {
                if (image_container.find('img').length > 0) {
                    let image_height = image_container.innerHeight();
                    switch (animation) {
                        case effect_random[0]:
                            let move_up_val = (((image_height - $(parent_container).outerHeight()) / image_height) * 100) * -1;
                            $(parent_container).css({
                                '--move_up_value': move_up_val + '%',
                                '--animation-duration': (10 * move_up_val) / -15 + 's'
                            });
                            break;
                        case effect_random[1]:
                            let move_dif = image_height - $(parent_container).outerHeight();
                            image_container.css('bottom', '0px');
                            let move_down_val = (move_dif / image_height) * 100;
                            $(parent_container).css({
                                '--move_down_value': move_down_val + '%',
                                '--animation-duration': (10 * move_down_val) / 15 + 's'
                            });
                            break;
                        default:
                    }
                }
            }

            function animate_random_start(element_container, elements, element_index, animations, animation_index) {
                let ele = $(elements[element_index]);
                let animation = animations[animation_index];
                set_transform_values(element_container, ele, animation);
                ele.fadeIn().addClass(animation);
                ele.on('transitionend webkitTransitionEnd oTransitionEnd', function (e) {
                    animate_random_start(element_container, elements, get_index(element_index, elements.length), animations, get_index(animation_index, animations.length));
                    animate_random_end(ele, animation);
                    ele.unbind('transitionend webkitTransitionEnd oTransitionEnd');
                });
            }

            function init_random(event, slider_content, slider_setting) {
                let content = $(slider_content);
                animate_random_start(content, content.children(selector_slider_item), 0, effect_random, 0);
            }

            // ############################### GALLERY ################################## //
            function init_gallery(event, slider_content, slider_setting) {
                let delay = 200;
                $.when($(slider_content).children(selector_slider_item).each(function (index) {
                        $(this).delay(delay).queue(function () {
                            $(this).addClass(class_animate_animated + ' ' + class_animate_prefix + 'fadeIn').dequeue();
                        });
                        delay += slider_setting[transition_delay];
                    })
                ).then(function () {
                    $(slider_content).trigger(event_animation_end + ':' + slider_setting[effect_type] + slider_setting['slider_id ']);
                });
            }

            // ############################### INIT ################################## //
            function init(_settings = settings) {
                let mod_settings = _settings['jsys_content_slider'];
                Object.keys(mod_settings).forEach(function (key, index) {
                    let slider_setting = mod_settings[key];
                    let slider_content = $(selector_slider + '[slider-id=' + slider_setting[slider_id] + ']' + selector_slider_content);
                    slider_content.addClass(slider_setting[effect_type]);
                    slider_setting[transition_delay] = slider_setting[transition_delay] ? (slider_setting[transition_delay] * 1000) : 5000;
                    $(document, context).trigger(event_prefix + slider_setting[effect_type], [slider_content, slider_setting]);
                });
            }

            // Run only if slider exists and settings are ready
            //todo test if changes made here will work with custom block slider...
            if ($(selector_slider).length > 0 && settings['jsys_content_slider']) {
                init();
            }

            // ############################### INIT MANUALLY BY CUSTOM JS ################################## //
            function prep_slider(user_config) {
                let slider_selector = user_config['slider_selector'] || '#' + user_config[slider_id];
                $(slider_selector).attr('slider-id', user_config[slider_id]);
            }

            function init_manual(event, user_settings = event.detail) {
                prep_slider(user_settings);
                user_settings['slider_config'][slider_id] = user_settings[slider_id];
                let settings = {jsys_content_slider: {}};
                settings['jsys_content_slider'][user_settings[slider_id]] = user_settings['slider_config'];
                init(settings);
            }


            // Trigger slider is ready for use event
            $(document).trigger('jsys_slider_available');
            document.dispatchEvent(new CustomEvent('jsys_slider_available')); //In case pure js is used by users.
        }
    };
})(jQuery);