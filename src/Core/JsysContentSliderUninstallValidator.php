<?php


namespace Drupal\jsys_content_slider\Core;

use Drupal;
use Drupal\Core\Extension\ModuleUninstallValidatorInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\Url;


class JsysContentSliderUninstallValidator implements ModuleUninstallValidatorInterface {

    use StringTranslationTrait;

    /**
     * Constructs a new JsysContentSliderUninstallValidator.
     *
     * @param TranslationInterface $string_translation
     *   The string translation service.
     */
    public function __construct(TranslationInterface $string_translation) {
        $this->stringTranslation = $string_translation;
    }


    /**
     * {@inheritdoc}
     */
    public function validate($module) {
        $reasons = [];
        if ($module == Constant::MODULE) {
            $config = Drupal::entityTypeManager()->getStorage('block_content_type')->load($module);
            if (!empty($config)) {
                $reasons[] = $this->t('There is content or configuration for this block type. <a id="' . Constant::MODULE . '" href=":url">Remove content and configuration</a>.', [
                    ':url' => Url::fromRoute(Constant::MODULE . '.prepare_module_uninstall')->toString(),
                ]);
            }
        }
        return $reasons;
    }

}