<?php


namespace Drupal\jsys_content_slider\Core;

use Drupal;

/**
 * Class Module
 * Hold module context to share amongst templates.
 *
 * @package Drupal\jsys_content_slider
 */
class ModuleContext {

    private $slider_configs;

    private function __construct() {
        $this->slider_configs = [
            Constant::MODULE_DIR => Drupal::service('extension.list.module')->getPath(Constant::MODULE) . '/templates',
            Constant::MODULE_DYNAMIC_FIELDS => [Constant::CONTENT_TYPE_HTML, Constant::CONTENT_TYPE_IMAGES],
        ];
    }

    public function set($key, $value) {
        $this->slider_configs[$key] = $value;
        return $this;
    }

    public function add(array $values) {
        $this->slider_configs = array_merge($this->slider_configs, $values);
        return $this;
    }

    public function get($key) {
        return $this->slider_configs[$key];
    }

    public function remove($key) {
        if (isset($this->slider_configs[$key])) {
            unset($this->slider_configs[$key]);
        }
        return $this;
    }

    public function to_array() {
        return $this->slider_configs;
    }

    public function clear() {
        unset($this->slider_configs);
        self::$instance = null;
        return null;
    }

    private static $instance = null;

    public static function config() {
        if (self::$instance == null) {
            self::$instance = new ModuleContext();
        }
        return self::$instance;
    }
}