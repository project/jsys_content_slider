<?php

namespace Drupal\jsys_content_slider\Core;

use Drupal\Component\Utility\NestedArray;

class ModuleHelper {

    static function register_templates() {
        $path = ModuleContext::config()->get(Constant::MODULE_DIR);
        $theme = [];
        $theme['block__' . Constant::MODULE] = [
            'render element' => 'content',
            'base hook' => 'block',
            'template' => 'block--jsys-content-slider',
            'path' => $path,
        ];
        $theme['field__block_content__' . Constant::MODULE] = [
            'render element' => 'content',
            'base hook' => 'field',
            'template' => 'field--block-content--jsys-content-slider',
            'path' => $path,
        ];
        return $theme;
    }

    static function set_content_type_select_options(&$form) {
        $current_options = &$form[Constant::WIDGET_CONTENT_TYPE]['widget'][0][Constant::FIELD_VALUE]['#options'];
        foreach ($form as $key => $field) {
            if (strpos($key, 'field_') !== false) {
                $current_options[$key] = $field['widget']['#title'];
            }
        }
    }

    static function add_dynamic_field_wrappers(&$form) {
        $form[Constant::CONTENT_TYPE_IMAGES]['#attributes']['class'][] = Constant::DYNAMIC_FIELD . ' content-type ' . Constant::CONTENT_TYPE_IMAGES;
        $form[Constant::CONTENT_TYPE_HTML]['#attributes']['class'][] = Constant::DYNAMIC_FIELD . ' content-type ' . Constant::CONTENT_TYPE_HTML;
    }

    static function set_custom_slider_configs(&$variables, $block_content) {
        if ($block_content) {
            $custom_config = self::get_custom_config($block_content);
            $variables['#attached'] = self::update_attached($variables, $custom_config, $block_content->uuid());
            $variables['attributes'] = self::get_class($variables, $block_content->jsys_css_class->value);
            $variables['attributes']['slider-id'] = $custom_config[Constant::SLIDER_ID];
            ModuleContext::config()->add($custom_config);
        }
    }

    static function extract_nested_values($form, $form_state, $field_name, $delta, $parent_container) {
        $path = array_merge($form['#parents'], [$field_name]);
        return NestedArray::getValue($form_state->getValues(), $path)[$delta][$parent_container];
    }

    private static function get_custom_config($block_content) {
        return array_merge([
            Constant::SLIDER_ID => rand(0, 9999),
            Constant::CONTENT_TYPE => $block_content->jsys_content_type->value,
            Constant::EFFECT_TRANSITION_DELAY_ID => $block_content->jsys_effect_type->transition_delay,
        ], self::get_effect_type($block_content->jsys_effect_type));
    }

    private static function get_effect_type($effect_type) {
        $effect_type_value = $effect_type->value;
        $config = [
            Constant::EFFECT_TYPE => $effect_type_value,
            Constant::EFFECT_TYPE_VALUE => $effect_type_value,
        ];
        switch ($effect_type_value) {
            case Constant::EFFECT_TYPE_GALLERY:
            case Constant::EFFECT_TYPE_RANDOM:
                break;
            case Constant::EFFECT_TYPE_FIELD_ID_CUSTOM:
                $config[Constant::EFFECT_TYPE] = Constant::EFFECT_TYPE_ANIMATION;
                $config[Constant::EFFECT_TYPE_VALUE] = $effect_type->custom_effect;
                break;
            default:
                $config[Constant::EFFECT_TYPE] = Constant::EFFECT_TYPE_ANIMATION;
                $config[Constant::EFFECT_TYPE_VALUE] = $effect_type_value;
        }
        return $config;
    }

    private static function update_attached($variables, $custom_config, $uuid) {
        $attachments = isset($variables['#attached']) ? $variables['#attached'] : [];
        $attachments['drupalSettings'] = isset($attachments['drupalSettings']) ? $attachments['drupalSettings'] : [];
        $attachments['drupalSettings'][Constant::MODULE] = isset($attachments['drupalSettings'][Constant::MODULE]) ? $attachments['drupalSettings'][Constant::MODULE] : [];
        $attachments['drupalSettings'][Constant::MODULE][$uuid] = $custom_config;
        return $attachments;
    }

    private static function get_class($variables, $block_class) {
        $attributes = isset($variables['attributes']) ? $variables['attributes'] : [];
        $attributes['class'] = isset($attributes['class']) ? $attributes['class'] : [];
        array_push($attributes['class'], $block_class);
        return $attributes;
    }

}