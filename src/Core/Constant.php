<?php


namespace Drupal\jsys_content_slider\Core;


class Constant {

    const MODULE_PREFIX = 'jsys_';
    const MODULE = self::MODULE_PREFIX.'content_slider';
    const MODULE_DIR = 'directory';
    const MODULE_CONFIG = self::MODULE_PREFIX.'slider_config';
    const MODULE_DYNAMIC_FIELDS = 'dynamic_fields';

    const SLIDER_ID = 'slider_id';
    const FIELD_VALUE = 'value';
    const DYNAMIC_FIELD = 'dynamic-field';
    const EFFECT_TYPE = 'effect_type';
    const EFFECT_TYPE_VALUE = self::EFFECT_TYPE.'_'.self::FIELD_VALUE;
    const CONTENT_TYPE = 'content_type';

    const WIDGET_CONTENT_TYPE = self::MODULE_PREFIX.self::CONTENT_TYPE;
    const WIDGET_EFFECT_TYPE = self::MODULE_PREFIX.self::EFFECT_TYPE;
    const WIDGET_SUMMARY = self::MODULE_PREFIX.'summary';

    const CONTENT_TYPE_FIELD_LABEL = 'Slider Content Type';
    const CONTENT_TYPE_HTML = self::MODULE_PREFIX.'html';
    const CONTENT_TYPE_IMAGES = self::MODULE_PREFIX.'images';

    const EFFECT_TYPE_FIELD_LABEL = 'Slider Effect Type';
    const EFFECT_TYPE_FIELD_ID_CUSTOM = 'custom_effect';
    const EFFECT_TYPE_FIELD_CUSTOM_LABEL = 'Custom Slider Effect Type';
    const EFFECT_TRANSITION_DELAY_ID = 'transition_delay';
    const EFFECT_TRANSITION_DELAY_LABEL = 'Transition delay';
    const EFFECT_TYPE_GALLERY = 'gallery';
    const EFFECT_TYPE_RANDOM = 'random';
    const EFFECT_TYPE_ANIMATION = 'animation';

    const SUMMARY_FIELD_LABEL = 'Summary';
    const SUMMARY_ENABLED_FIELD_ID = 'enabled';
    const SUMMARY_ENABLED_FIELD_LABEL = 'Show '.self::SUMMARY_FIELD_LABEL;

    const CSS_CLASS_FIELD_LABEL = 'Custom CSS class';

}