<?php


namespace Drupal\jsys_content_slider\Form;

use Drupal;
use Drupal\jsys_content_slider\Core\Constant;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityDeleteForm;
use Drupal\Core\Url;

/**
 * Class JsysContentSliderUninstallForm
 *
 * Confirm deletion of all data before uninstalling module.
 *
 * @package Drupal\jsys_content_slider\Form
 */
class JsysContentSliderUninstallForm extends EntityDeleteForm {

    const ENTITY_TYPE = 'block_content';
    const BASE_IMAGE_FOLDER_PATH = 'sites/default/files/' . Constant::MODULE;

    private $block_content_storage;
    private $block_content_type_storage;
    private $block_content_type_entity;
    private $block_content_ids;

    /**
     * {@inheritdoc}
     */
    public function getCancelUrl() {
        return new Url('system.modules_uninstall');
    }

    /**
     * {@inheritdoc}
     */
    public function getEntity() {
        return $this->block_content_type_entity;
    }

    /**
     * {@inheritdoc}
     */
    public function getFormId() {
        return Constant::MODULE . '_confirm_data_delete_form';
    }

    /**
     * {@inheritdoc}
     */
    public function getBaseFormId() {
        return $this->getFormId();
    }

    /**
     * {@inheritdoc}
     */
    public function getQuestion() {
        return t('Are you sure you want to delete all data for this module?');
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription() {
        //We will set the description directly in the form where needed.
        return '';
    }

    /**
     * {@inheritdoc}
     */
    protected function prepareInvokeAll($hook, FormStateInterface $form_state) {
        //We do not need to invoke any hooks.
    }

    /**
     * {@inheritdoc}
     */
    protected function init(FormStateInterface $form_state) {
        if (!$form_state->has('entity_form_initialized')) {
            $this->prep_context($form_state);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
        $this->prep_context($form_state);

        if ($this->has_block_content()) {
            $form['delete_data_details'] = [
                '#type' => 'details',
                '#title' => t('Data deletions'),
                '#description' => t('The listed blocks will be deleted.'),
                '#open' => true,
            ];
            $form['delete_data_details']['delete_data'] = [
                '#type' => 'html_tag',
                '#tag' => 'ul',
                '#value' => $this->get_block_content(),
            ];
        }

        $form = parent::buildForm($form, $form_state);

        if (file_exists('public://' . Constant::MODULE)) {
            $form['delete_files_details'] = [
                '#type' => 'details',
                '#title' => t('Manual deletions (optional)'),
                '#description' => t('The image folder at path: <code>' . self::BASE_IMAGE_FOLDER_PATH . '</code> is associated with this module but will not be deleted, as it does not require the module to be installed.<br> You may choose to delete it manually later.'),
                '#open' => true,
            ];
        }

        $form['delete_back_up'] = [
            '#type' => 'html_tag',
            '#tag' => 'div',
            '#value' => t('This action cannot be undone. Back up your database if you want to be able to restore the data in the future.'),
        ];
        return $form;
    }


    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        $blocks = $this->block_content_storage->loadMultiple($this->block_content_ids);
        $this->block_content_storage->delete($blocks);
        $this->block_content_type_storage->delete([$this->block_content_type_entity]);
        $form_state->cleanValues();
        $form_state->setRedirectUrl($this->getCancelUrl());
        Drupal::messenger()->addMessage(t('Deletion completed successfully. You may uninstall the module now.'));
    }

    private function prep_context(&$form_state) {
        $this->entityTypeId = Constant::MODULE;
        $this->entityTypeManager = Drupal::entityTypeManager();
        $this->block_content_storage = $this->entityTypeManager->getStorage(self::ENTITY_TYPE);
        $this->block_content_type_storage = $this->entityTypeManager->getStorage(self::ENTITY_TYPE . '_type');
        $this->block_content_type_entity = $this->block_content_type_storage->load(Constant::MODULE);

        //Used by parent forms.
        $this->entity = $this->block_content_type_entity;
        $form_state->set('entity_form_initialized', TRUE);
    }

    private function has_block_content() {
        $this->block_content_ids = $this->block_content_storage->getQuery()->condition('type', Constant::MODULE)->execute();
        return !empty($this->block_content_ids) && count($this->block_content_ids) > 0;
    }

    private function get_block_content() {
        $html = '';
        $blocks = $this->block_content_storage->loadMultiple($this->block_content_ids);
        foreach ($blocks as $id => $block) {
            $html .= '<li>' . $block->toLink()->toString() . '</li>';
        }
        return $html;
    }
}