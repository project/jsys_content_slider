<?php

/**
 * @file
 * Contains \Drupal\jsys_content_slider\ProxyClass\JsysContentSliderUninstallValidator.
 */

/**
 * This file was generated via php core/scripts/generate-proxy-class.php 'Drupal\jsys_content_slider\JsysContentSliderUninstallValidator' "modules/contrib/features/src".
 */

namespace Drupal\jsys_content_slider\ProxyClass\Core {

  use \Drupal\Core\Extension\ModuleUninstallValidatorInterface;
  /**
   * Provides a proxy class for \Drupal\jsys_content_slider\JsysContentSliderUninstallValidator.
   *
   * @see \Drupal\Component\ProxyBuilder
   */
  class JsysContentSliderUninstallValidator implements ModuleUninstallValidatorInterface
  {

    use \Drupal\Core\DependencyInjection\DependencySerializationTrait;

    /**
     * The id of the original proxied service.
     *
     * @var string
     */
    protected $drupalProxyOriginalServiceId;

    /**
     * The real proxied service, after it was lazy loaded.
     *
     * @var \Drupal\jsys_content_slider\JsysContentSliderUninstallValidator
     */
    protected $service;

    /**
     * The service container.
     *
     * @var \Symfony\Component\DependencyInjection\ContainerInterface
     */
    protected $container;

    /**
     * Constructs a ProxyClass Drupal proxy object.
     *
     * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
     *   The container.
     * @param string $drupal_proxy_original_service_id
     *   The service ID of the original service.
     */
    public function __construct(\Symfony\Component\DependencyInjection\ContainerInterface $container, $drupal_proxy_original_service_id)
    {
      $this->container = $container;
      $this->drupalProxyOriginalServiceId = $drupal_proxy_original_service_id;
    }

    /**
     * Lazy loads the real service from the container.
     *
     * @return object
     *   Returns the constructed real service.
     */
    protected function lazyLoadItself()
    {
      if (!isset($this->service)) {
        $this->service = $this->container->get($this->drupalProxyOriginalServiceId);
      }

      return $this->service;
    }

    /**
     * {@inheritdoc}
     */
    public function validate($module)
    {
      return $this->lazyLoadItself()->validate($module);
    }

  }

}
