<?php

namespace Drupal\jsys_content_slider\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Hidden field formatter.
 *
 * @FieldFormatter(
 *   id = "jsys_hidden_field_formatter",
 *   label = @Translation("Hidden"),
 *   field_types = {
 *     "jsys_effect_type",
 *     "jsys_content_type",
 *     "jsys_css_class",
 *   }
 * )
 */
class HiddenFieldFormatter extends FormatterBase {

    /**
     * {@inheritdoc}
     */
    public function viewElements(FieldItemListInterface $items, $langcode) {
        return [];
    }

}