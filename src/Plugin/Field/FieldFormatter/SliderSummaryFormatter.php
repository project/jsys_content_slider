<?php

namespace Drupal\jsys_content_slider\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Slider Effect Type field formatter.
 *
 * @FieldFormatter(
 *   id = "jsys_summary_formatter",
 *   label = @Translation("Slider Summary"),
 *   field_types = {
 *     "jsys_summary"
 *   }
 * )
 */
class SliderSummaryFormatter extends FormatterBase {

    /**
     * {@inheritdoc}
     */
    public function viewElements(FieldItemListInterface $items, $langcode) {
        $result = [];
        if (count($items) > 0) {
            foreach ($items as $delta => $item) {
                if ($item->enabled == 1) {
                    $result[$delta] = [
                        '#markup' => $item->value,
                    ];
                }
            }
        }
        return $result;
    }

}