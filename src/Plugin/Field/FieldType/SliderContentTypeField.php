<?php

namespace Drupal\jsys_content_slider\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\jsys_content_slider\Core\Constant;

/**
 * The definition of the Slider Content Type field type.
 *
 * @FieldType(
 *   id = "jsys_content_type",
 *   label = @Translation("Slider Content Type"),
 *   description = @Translation("Provides a Slider Content Type field."),
 *   category = @Translation("Jsys Content Slider"),
 *   default_widget = "jsys_content_type_widget",
 *   default_formatter = "jsys_hidden_field_formatter"
 * )
 */
class SliderContentTypeField extends FieldItemBase {

    /**
     * Field type properties definition.
     * List of allowed property types: https://goo.gl/sIBBgO
     */
    public static function propertyDefinitions(FieldStorageDefinitionInterface $storage) {
        $properties[Constant::FIELD_VALUE] = DataDefinition::create('string')
            ->setLabel(t(Constant::CONTENT_TYPE_FIELD_LABEL))
            ->setRequired(TRUE);
        return $properties;
    }

    /**
     * Defines the database schema used to store data for the CSS field type.
     * List of allowed column types: https://goo.gl/YY3G7s
     * Data types: https://www.drupal.org/node/159605
     */
    public static function schema(FieldStorageDefinitionInterface $storage) {
        $columns = [];
        $columns[Constant::FIELD_VALUE] = [
            'type' => 'text',
            'size' => 'normal',
        ];
        return [
            'columns' => $columns,
            'indexes' => [],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function isEmpty() {
        return empty($this->get(Constant::FIELD_VALUE)->getValue());
    }
}