<?php

namespace Drupal\jsys_content_slider\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\jsys_content_slider\Core\Constant;

/**
 * The definition of the Slider Effect Type field type.
 *
 * @FieldType(
 *   id = "jsys_effect_type",
 *   label = @Translation("Slider Effect Type"),
 *   description = @Translation("Provides a Slider Effect Type field."),
 *   category = @Translation("Jsys Content Slider"),
 *   default_widget = "jsys_effect_type_widget",
 *   default_formatter = "jsys_hidden_field_formatter"
 * )
 */
class SliderEffectTypeField extends FieldItemBase {

    /**
     * Field type properties definition.
     * List of allowed property types: https://goo.gl/sIBBgO
     */
    public static function propertyDefinitions(FieldStorageDefinitionInterface $storage) {
        $properties[Constant::FIELD_VALUE] = DataDefinition::create('string')
            ->setLabel(t(Constant::EFFECT_TYPE_FIELD_LABEL))
            ->setRequired(true);
        $properties[Constant::EFFECT_TYPE_FIELD_ID_CUSTOM] = DataDefinition::create('string')
            ->setLabel(t(Constant::EFFECT_TYPE_FIELD_CUSTOM_LABEL))
            ->setRequired(false);
        $properties[Constant::EFFECT_TRANSITION_DELAY_ID] = DataDefinition::create('float')
            ->setLabel(t(Constant::EFFECT_TRANSITION_DELAY_LABEL))
            ->setRequired(false);
        return $properties;
    }

    /**
     * Defines the database schema used to store data for the CSS field type.
     * List of allowed column types: https://goo.gl/YY3G7s
     * Data types: https://www.drupal.org/node/159605
     */
    public static function schema(FieldStorageDefinitionInterface $storage) {
        $columns = [];
        $columns[Constant::FIELD_VALUE] = [
            'type' => 'text',
            'size' => 'normal',
        ];
        $columns[Constant::EFFECT_TYPE_FIELD_ID_CUSTOM] = [
            'type' => 'text',
            'size' => 'normal',
        ];
        $columns[Constant::EFFECT_TRANSITION_DELAY_ID] = [
            'type' => 'float',
            'size' => 'normal',
        ];
        return [
            'columns' => $columns,
            'indexes' => [],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function isEmpty() {
        $value1 = $this->get(Constant::FIELD_VALUE)->getValue();
        $value2 = $this->get(Constant::EFFECT_TYPE_FIELD_ID_CUSTOM)->getValue();
        return empty($value1) || ($value1 === Constant::EFFECT_TYPE_FIELD_ID_CUSTOM && empty($value2));
    }
}