<?php

namespace Drupal\jsys_content_slider\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\jsys_content_slider\Core\Constant;

/**
 * The definition of the Custom CSS class field type.
 *
 * @FieldType(
 *   id = "jsys_css_class",
 *   label = @Translation("Custom CSS class"),
 *   description = @Translation("Provides a Custom CSS class field."),
 *   category = @Translation("Jsys Content Slider"),
 *   default_widget = "jsys_css_class_widget",
 *   default_formatter = "jsys_hidden_field_formatter"
 * )
 */
class CssClassField extends FieldItemBase {

    /**
     * Field type properties definition.
     * List of allowed property types: https://goo.gl/sIBBgO
     */
    public static function propertyDefinitions(FieldStorageDefinitionInterface $storage) {
        $properties[Constant::FIELD_VALUE] = DataDefinition::create('string')
            ->setLabel(t(Constant::CSS_CLASS_FIELD_LABEL))
            ->setRequired(false);
        return $properties;
    }

    /**
     * Defines the database schema used to store data for the CSS field type.
     * List of allowed column types: https://goo.gl/YY3G7s
     * Data types: https://www.drupal.org/node/159605
     */
    public static function schema(FieldStorageDefinitionInterface $storage) {
        $columns = [];
        $columns[Constant::FIELD_VALUE] = [
            'type' => 'text',
            'size' => 'normal',
        ];
        return [
            'columns' => $columns,
            'indexes' => [],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function isEmpty() {
        return empty($this->get(Constant::FIELD_VALUE)->getValue());
    }
}