<?php

namespace Drupal\jsys_content_slider\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\jsys_content_slider\Core\Constant;

/**
 * The definition of the Slider Summary field type.
 *
 * @FieldType(
 *   id = "jsys_summary",
 *   label = @Translation("Slider Summary"),
 *   description = @Translation("Provides a Slider Summary field."),
 *   category = @Translation("Jsys Content Slider"),
 *   default_widget = "jsys_summary_widget",
 *   default_formatter = "jsys_summary_formatter"
 * )
 */
class SliderSummaryField extends FieldItemBase {

    /**
     * Field type properties definition.
     * List of allowed property types: https://goo.gl/sIBBgO
     */
    public static function propertyDefinitions(FieldStorageDefinitionInterface $storage) {
        $properties[Constant::FIELD_VALUE] = DataDefinition::create('string')
            ->setLabel(t(Constant::SUMMARY_FIELD_LABEL))
            ->setRequired(false);
        $properties[Constant::SUMMARY_ENABLED_FIELD_ID] = DataDefinition::create('integer')
            ->setLabel(t(Constant::SUMMARY_ENABLED_FIELD_LABEL))
            ->setRequired(false);
        return $properties;
    }

    /**
     * Defines the database schema used to store data for the CSS field type.
     * List of allowed column types: https://goo.gl/YY3G7s
     * Data types: https://www.drupal.org/node/159605
     */
    public static function schema(FieldStorageDefinitionInterface $storage) {
        $columns = [];
        $columns[Constant::FIELD_VALUE] = [
            'type' => 'text',
            'size' => 'big',
        ];
        $columns[Constant::SUMMARY_ENABLED_FIELD_ID] = [
            'type' => 'int',
            'size' => 'tiny',
        ];

        return [
            'columns' => $columns,
            'indexes' => [],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function isEmpty() {
        $value1 = $this->get(Constant::SUMMARY_ENABLED_FIELD_ID)->getValue();
        $value2 = $this->get(Constant::FIELD_VALUE)->getValue();
        return $value1 == 0 || ($value1 === 1 && empty($value2));
    }
}