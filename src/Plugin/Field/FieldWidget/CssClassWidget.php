<?php

namespace Drupal\jsys_content_slider\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\jsys_content_slider\Core\Constant;

/**
 * CSS Class field widget definition.
 *
 * @FieldWidget(
 *   id = "jsys_css_class_widget",
 *   label = @Translation("CSS Class"),
 *   field_types = {
 *     "jsys_css_class"
 *   }
 * )
 */
class CssClassWidget extends WidgetBase {


    /**
     * Define the form used to edit the Slider Content Type field in the UI.
     * List of allowed element types: https://goo.gl/XVd4tA
     */
    public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

        $element[Constant::FIELD_VALUE] = [
            '#type' => 'textfield',
            '#title' => t(Constant::CSS_CLASS_FIELD_LABEL),
            '#default_value' => isset($items[$delta]->value) ? $items[$delta]->value : null,
            '#required' => false,
            '#description' => 'Enter custom css classes for the slider container. E.g.: my-class1 my-class2.'
        ];
        return $element;
    }
}