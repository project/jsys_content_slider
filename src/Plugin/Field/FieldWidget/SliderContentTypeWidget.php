<?php

namespace Drupal\jsys_content_slider\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\jsys_content_slider\Core\Constant;

/**
 * Slider Content Type field widget definition.
 *
 * @FieldWidget(
 *   id = "jsys_content_type_widget",
 *   label = @Translation("Slider Content Type"),
 *   field_types = {
 *     "jsys_content_type"
 *   }
 * )
 */
class SliderContentTypeWidget extends WidgetBase {


    /**
     * Define the form used to edit the Slider Content Type field in the UI.
     * List of allowed element types: https://goo.gl/XVd4tA
     */
    public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
        $element[Constant::FIELD_VALUE] = [
            '#type' => 'select',
            '#title' => t(Constant::CONTENT_TYPE_FIELD_LABEL),
            '#default_value' => isset($items[$delta]->value) ? $items[$delta]->value : Constant::CONTENT_TYPE_IMAGES,
            '#options' => [
                Constant::CONTENT_TYPE_IMAGES => 'Image',
                Constant::CONTENT_TYPE_HTML => 'Custom HTML',
            ],
            '#required' => TRUE,
            '#description' => t('What kind of content will be displayed in this slider?'),
            '#attributes' => [
                'class' => [Constant::WIDGET_CONTENT_TYPE],
            ],
        ];
        return $element;
    }

}