<?php

namespace Drupal\jsys_content_slider\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\jsys_content_slider\Core\Constant;

/**
 * Slider Effect Type field widget definition.
 *
 * @FieldWidget(
 *   id = "jsys_effect_type_widget",
 *   label = @Translation("Slider Effect Type"),
 *   field_types = {
 *     "jsys_effect_type"
 *   }
 * )
 */
class SliderEffectTypeWidget extends WidgetBase {

    /**
     * {@inheritdoc}
     *
     * Define the form used to edit the Slider Content Type field in the UI.
     * List of allowed element types: https://goo.gl/XVd4tA
     *
     */
    public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

        $value = $items[$delta]->value;
        $value_custom = $items[$delta]->custom_effect;
        $value_delay = $items[$delta]->transition_delay;

        $element += [
            '#type' => 'details',
            '#title' => t(Constant::EFFECT_TYPE_FIELD_LABEL),
            '#description' => t('Which effect should be applied to the slider content?'),
            '#open' => true,
        ];

        $element[Constant::FIELD_VALUE] = [
            '#type' => 'select',
            '#default_value' => isset($value) ? $value : Constant::EFFECT_TYPE_GALLERY,
            '#options' => [
                Constant::EFFECT_TYPE_GALLERY => 'Content gallery',
                'zoomIn_slideOutLeft' => 'Zoom in - Slide out left',
                'slideInRight_slideOutLeft' => 'Slide in right - Slide out left',
                Constant::EFFECT_TYPE_RANDOM => 'Random effects',
                Constant::EFFECT_TYPE_FIELD_ID_CUSTOM => 'Custom effect',
            ],
            '#required' => true,
            '#attributes' => [
                'class' => [Constant::WIDGET_EFFECT_TYPE],
            ],
        ];

        $element[Constant::EFFECT_TYPE_FIELD_ID_CUSTOM] = [
            '#type' => 'textfield',
            '#title' => t(Constant::EFFECT_TYPE_FIELD_CUSTOM_LABEL),
            '#default_value' => isset($value_custom) ? $value_custom : null,
            '#required' => (isset($value) && $value === Constant::EFFECT_TYPE_FIELD_ID_CUSTOM),
            '#description' => t('See the module documentation to find out how to create custom effects.'),
            '#wrapper_attributes' => [
                'class' => [
                    Constant::DYNAMIC_FIELD,
                    'effect-type',
                    Constant::EFFECT_TYPE_FIELD_ID_CUSTOM
                ],
            ],
        ];

        $element[Constant::EFFECT_TRANSITION_DELAY_ID] = [
            '#type' => 'number',
            '#title' => t(Constant::EFFECT_TRANSITION_DELAY_LABEL),
            '#default_value' => isset($value_delay) ? $value_delay : 5,
            '#required' => false,
            '#min' => 0,
            '#step' => 0.01,
            '#description' => t('The amount of seconds to wait before starting the animations for the next content item.'),
            '#wrapper_attributes' => [
                'class' => [
                    'delay_field'
                ],
            ],
        ];

        return $element;
    }
}