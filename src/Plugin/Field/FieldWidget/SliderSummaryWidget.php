<?php

namespace Drupal\jsys_content_slider\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\jsys_content_slider\Core\Constant;

/**
 * Slider Effect Type field widget definition.
 *
 * @FieldWidget(
 *   id = "jsys_summary_widget",
 *   label = @Translation("Slider Summary"),
 *   field_types = {
 *     "jsys_summary"
 *   }
 * )
 */
class SliderSummaryWidget extends WidgetBase {

    /**
     * Define the form used to edit the Slider Content Type field in the UI.
     * List of allowed element types: https://goo.gl/XVd4tA
     */
    public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

        $value = $items[$delta]->value;
        $value_enabled = $items[$delta]->enabled;

        $element += [
            '#type' => 'details',
            '#title' => $this->t(Constant::SUMMARY_FIELD_LABEL),
            '#description' => t('Add a summary to the slider.'),
            '#open' => true,
        ];

        $element[Constant::SUMMARY_ENABLED_FIELD_ID] = [
            '#type' => 'checkbox',
            '#title' => t(Constant::SUMMARY_ENABLED_FIELD_LABEL),
            '#default_value' => isset($value_enabled) ? $value_enabled : 0,
            '#required' => false,
            '#return_value' => 1,
            '#attributes' => [
                'class' => [Constant::WIDGET_SUMMARY . '_' . Constant::SUMMARY_ENABLED_FIELD_ID],
            ],
        ];
        $element[Constant::FIELD_VALUE] = [
            '#type' => 'textarea',
            '#title' => t(Constant::SUMMARY_FIELD_LABEL),
            '#default_value' => isset($value) ? $value : null,
            '#required' => (isset($value_enabled) && $value_enabled === 1),
            '#rows' => 3,
            '#wrapper_attributes' => [
                'class' => [
                    Constant::DYNAMIC_FIELD,
                    Constant::WIDGET_SUMMARY
                ],
            ],
        ];
        return $element;
    }

}