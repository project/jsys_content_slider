<h3>About</h3>
<p>This module provides content sliders using custom blocks. It can be used with all field types (content reference as well). All custom block functionality is supported.</p>
<br>
<hr />
<h3>Getting started</h3>
<ol>
<li>Navigate to the custom blocks page and create a Jsys Content Slider block.</li>
<li>Select the type of data that will be displayed in the slider (image is default).</li>
<li>Place the block in the UI.</li>
</ol>

<h3>Custom content types</h3>
<ol>
<li>Add the wanted field to the Jsys Content Slider block type definition. You do not need to delete the default image field.</li>
<li>Create a Jsys Content Slider block and select the created field as Content Type for the slider.</li>
</ol>

<h3>Custom effect types</h3>
<p>This module uses <a href="https://animate.style/">Animate.css</a> to provide some of its animations. You may use custom animations from Animate.css or create your own animations to use with the slider. Follow the instructions below to create custom animations.
<ol>
<li>When creating a content slider, select Custom effect in the Slider Effect Type section.</li>
<li>Type the effect name in the custom effect type text field using the following syntax: {effectIn}_{effectOut}.Eg.: <code>zoomIn_slideOutLeft</code>.<br>Note! Do <strong>not</strong> prepend the <code>animate__</code> prefix or use the <code>animate__animated</code> classes when using custom Animate.css effects. This will be done automatically by the slider. </li>
<li>To create your own effect use the same naming convention (e.g. myEffectIn_myEffectOut) then add the wanted transitions and animations using CSS.</li>
<li>The effect out suffix is optional.</li>
</ol>

<hr />
<h3>Manual markup</h3>
You may need to create a slider manually for example when using it with views content or views block. This can be done as follows:
<ol>
<li>Add the required HTML markup to your content:
<pre class=" language-markup"><code class="  language-markup"><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">id</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>my-slider-1<span class="token punctuation">"</span></span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span><strong>jsys-slider-container</strong> my-slider-1-class<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span><strong>jsys-slider-content</strong><span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span><strong>jsys-slider-item</strong><span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>My content 1<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span>
        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span><strong>jsys-slider-item</strong><span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>My content 2<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span>
    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span></code></pre>
Either a unique <strong>id</strong> or a unique <strong>class</strong> is required in order to identify this slider (supports multiple sliders on a page).<br>
</li>
<li>Initiate the slider using JavaScript or jQuery.<br>
<strong>JavaScript example</strong>
<pre class=" language-markup"><code class="  language-javascript"><span class="token punctuation">(</span><span class="token keyword">function</span> <span class="token punctuation">(</span><span class="token punctuation">)</span> <span class="token punctuation">{</span>
    <span class="token keyword">let</span> config <span class="token operator">=</span> <span class="token punctuation">{</span>
        slider_id<span class="token operator">:</span> <span class="token string">'my-slider-1'</span><span class="token punctuation">,</span> <span class="token comment">// required</span>
        slider_selector<span class="token operator">:</span> <span class="token string">'.my-slider-1-class'</span><span class="token punctuation">,</span> <span class="token comment">// optional, if not present the slider_id value (e.g. #my-slider-1) will be used to select the slider.</span>
        slider_config<span class="token operator">:</span> <span class="token punctuation">{</span>
            effect_type<span class="token operator">:</span> <span class="token string">'animation'</span><span class="token punctuation">,</span> <span class="token comment">// required, valid values: animation, random or gallery</span>
            effect_type_value<span class="token operator">:</span> <span class="token string">'{effectIn}_{effectOut}'</span><span class="token punctuation">,</span>  <span class="token comment">// required when effect_type = animation (see Custom effect types section above).</span>
            transition_delay<span class="token operator">:</span> <span class="token number">3</span> <span class="token comment">// optional delay in sections, default is 5 seconds. This option is not applicable (ignored) when effect_type = random</span>
        <span class="token punctuation">}</span>
    <span class="token punctuation">}</span><span class="token punctuation">;</span>
    document<span class="token punctuation">.</span><span class="token function">dispatchEvent</span><span class="token punctuation">(</span><span class="token keyword">new</span> <span class="token class-name">CustomEvent</span><span class="token punctuation">(</span><span class="token string">'init_jsys_slider'</span><span class="token punctuation">,</span> <span class="token punctuation">{</span>detail<span class="token operator">:</span> config<span class="token punctuation">}</span><span class="token punctuation">)</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
<span class="token punctuation">}</span><span class="token punctuation">)</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">;</span></code></pre>
<strong>jQuery example</strong>
<pre class=" language-markup"><code class="  language-javascript"><span class="token punctuation">(</span><span class="token keyword">function</span> <span class="token punctuation">(</span><span class="token parameter">$</span><span class="token punctuation">)</span> <span class="token punctuation">{</span>
    <span class="token keyword">let</span> config <span class="token operator">=</span> <span class="token punctuation">{</span> slider_id<span class="token operator">:</span> <span class="token string">'my-slider-1'</span><span class="token punctuation">,</span> <span class="token operator">...</span> <span class="token punctuation">}</span><span class="token punctuation">;</span>
    <span class="token function">$</span><span class="token punctuation">(</span>document<span class="token punctuation">)</span><span class="token punctuation">.</span><span class="token function">trigger</span><span class="token punctuation">(</span><span class="token string">'init_jsys_slider'</span><span class="token punctuation">,</span> <span class="token punctuation">[</span>config<span class="token punctuation">]</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
<span class="token punctuation">}</span><span class="token punctuation">)</span><span class="token punctuation">(</span>jQuery<span class="token punctuation">)</span><span class="token punctuation">;</span></code></pre>
</li>
</ol>
<strong>Note!</strong> Your script may be triggered before the slider js is avalaible. In this case you can wrap your script in an event listener so it can be triggered once the slider js is available.<br>
<strong>JavaScript example</strong>
<pre class=" language-markup"><code class="  language-javascript"><span class="token punctuation">(</span><span class="token keyword">function</span> <span class="token punctuation">(</span><span class="token punctuation">)</span> <span class="token punctuation">{</span>
    document<span class="token punctuation">.</span><span class="token function">addEventListener</span><span class="token punctuation">(</span><span class="token string">'jsys_slider_available'</span><span class="token punctuation">,</span> <span class="token keyword">function</span> <span class="token punctuation">(</span><span class="token parameter">e</span><span class="token punctuation">)</span> <span class="token punctuation">{</span>
        <span class="token keyword">let</span> config <span class="token operator">=</span> <span class="token punctuation">{</span>slider_id<span class="token operator">:</span> <span class="token string">'my-slider-1'</span><span class="token punctuation">,</span> <span class="token operator">...</span><span class="token punctuation">}</span><span class="token punctuation">;</span>
        document<span class="token punctuation">.</span><span class="token function">dispatchEvent</span><span class="token punctuation">(</span><span class="token keyword">new</span> <span class="token class-name">CustomEvent</span><span class="token punctuation">(</span><span class="token string">'init_jsys_slider'</span><span class="token punctuation">,</span> <span class="token punctuation">{</span>detail<span class="token operator">:</span> config<span class="token punctuation">}</span><span class="token punctuation">)</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
    <span class="token punctuation">}</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
<span class="token punctuation">}</span><span class="token punctuation">)</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">;</span></code></pre>
<strong>jQuery example</strong>
<pre class=" language-markup"><code class="  language-javascript"><span class="token punctuation">(</span><span class="token keyword">function</span> <span class="token punctuation">(</span><span class="token parameter">$</span><span class="token punctuation">)</span> <span class="token punctuation">{</span>
    <span class="token function">$</span><span class="token punctuation">(</span>document<span class="token punctuation">)</span><span class="token punctuation">.</span><span class="token function">on</span><span class="token punctuation">(</span><span class="token string">'jsys_slider_available'</span><span class="token punctuation">,</span> <span class="token keyword">function</span> <span class="token punctuation">(</span><span class="token punctuation">)</span> <span class="token punctuation">{</span>
        <span class="token keyword">let</span> config <span class="token operator">=</span> <span class="token punctuation">{</span>slider_id<span class="token operator">:</span> <span class="token string">'my-slider-1'</span><span class="token punctuation">,</span> <span class="token operator">...</span><span class="token punctuation">}</span><span class="token punctuation">;</span>
        <span class="token function">$</span><span class="token punctuation">(</span>document<span class="token punctuation">)</span><span class="token punctuation">.</span><span class="token function">trigger</span><span class="token punctuation">(</span><span class="token string">'init_jsys_slider'</span><span class="token punctuation">,</span> <span class="token punctuation">[</span>config<span class="token punctuation">]</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
    <span class="token punctuation">}</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
<span class="token punctuation">}</span><span class="token punctuation">)</span><span class="token punctuation">(</span>jQuery<span class="token punctuation">)</span><span class="token punctuation">;</span></code></pre>